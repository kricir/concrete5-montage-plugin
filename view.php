<?php
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('file_set');
Loader::model('file_list');
$fs = Fileset::getByID($fileset);
$fileList = new FileList();
$fileList->filterBySet($fs);
$files = $fileList->get();
?>


<div id="myCarousel" class="carousel slide">
    <!-- Carousel items -->
    <div class="carousel-inner">
        <?php
            foreach ($files as $f) {
        ?>
            <div class="item">
                <img src="<?php echo $f->getRelativePath(); ?>" />
                <div class="carousel-caption">
                    <p class="caption"><?php echo $f->getDescription(); ?></p>
                </div>
            </div>
        <?php
            }
        ?>
    </div>
    <!-- Carousel nav -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
</div>