<?php
defined('C5_EXECUTE') or die("Access Denied.");
Loader::model('file_set');
$s1 = FileSet::getMySets();
$sets = array();
foreach ($s1 as $s){
    $sets[$s->fsID] = $s->fsName;
}
$fsInfo['fileSets'] = $sets;
?>
<div class="setup-form">
	<div class="question">
	<?php echo $form->label('fileset', t('Please Choose an Imageset')); ?>
	<?php echo $form->select('fileset', $fsInfo['fileSets'],$fileset); ?>
	</div>
</div>

