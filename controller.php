<?php
defined('C5_EXECUTE') or die("Access Denied.");
class CarouselBlockController extends BlockController {
	protected $btTable = "btCarousel";
	protected $btInterfaceWidth = "550";
	protected $btInterfaceHeight = "300";

	public function getBlockTypeName() {
		return t('Bootstrap Carousel');
	}
	public function getBlockTypeDescription() {
		return t('Add a bootstrap carousel slideshow');
	}
}